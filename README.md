# Test_Deploiment_TravisCI_Python

[![Generic badge](https://img.shields.io/badge/For-Training-<green>.svg)](https://shields.io/)
[![Build](https://travis-ci.com/BoisselNicolas/Rils-deploy.svg?token=4kAo6qsZ5hqAksyhZUQD&branch=main)]()

## Contenu

1. Test de la CI de GitLab
1. Test unitaire Python

## Installation

```
$ git clone git@github.com:YTBeater/Test_Deploiment_TravisCI_Python.git
```

## Développé avec

* Python (unittest, math,)
* Gitlab-ci (python, pip,virtualenv , codecov, fichier de requirements)
* Travis-CI (python, pip, codecov, fichier de requirements)
 

https://www.linkedin.com/in/yann-picot-993140151/

## Auteurs

* **Beater** 
